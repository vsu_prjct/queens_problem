// Backtracking method. "����� � ���������" �� �������
// ������ � ������ ������.

#include <iostream>

using namespace std;

const int SIZE = 8; // ������.

int board[SIZE][SIZE];
int pos[SIZE];
int positions[100][SIZE] = { 0 };
int results_count = 0; // ���������� �������.


// ������� showBoard() - �������� ������� ����������� �������.
bool noReflected()
{
	bool noRef = true;
	for (int i = 0; i <= results_count; ++i)
	{
		for (int j = 0; j < SIZE; ++j){
			if (positions[i][j] != pos[SIZE - 1 - j]) break;
			if (j == SIZE - 1) return false;
		}
	}
	return true;
}

// ������� showBoard() - ���������� �����.
void showBoard()
{
	for (int a = 0; a < SIZE; ++a)
	{
		for (int b = 0; b < SIZE; ++b)
		{
			cout << ((board[a][b]) ? "Q " : ". ");
		}
		cout << '\n';
	}
}
void showRefBoard()
{
	for (int a = 0; a < SIZE; ++a)
	{
		for (int b = 0; b < SIZE; ++b)
		{
			cout << ((board[SIZE - 1 - a][b]) ? "Q " : ". ");
		}
		cout << '\n';
	}
}

// ������� tryQueen() - ��������� ��� �� ��� ������������� ������,
// �� ���������, ����������.
bool tryQueen(int a, int b)
{
	for (int i = 0; i < a; ++i)
	{
		if (board[i][b])
		{
			return false;
		}
	}

	for (int i = 1; i <= a && b - i >= 0; ++i)
	{
		if (board[a - i][b - i])
		{
			return false;
		}
	}

	for (int i = 1; i <= a && b + i < SIZE; i++)
	{
		if (board[a - i][b + i])
		{
			return false;
		}
	}

	return true;
}

// ������� setQueen() - ������� ����� ���������� �������.
void setQueen(int a) // a - ����� ��������� ������ � ������� ����� ��������� ���������� �����.
{
	if (a == SIZE)
	{
		if (noReflected()){
			showBoard();
			cout << endl;
			showRefBoard();
			memcpy(positions[results_count], pos, sizeof(positions[results_count]));
			cout << "Result #" << ++results_count << "\n\n";			
		}
		return; // �����������.
	}

	for (int i = 0; i < SIZE; ++i)
	{
		// ����� ���������, ��� ���� �������� � board[a][i] ����� (�������),
		// �� �� ����� ������������ � ���� ������, ������� � ����������.
		if (tryQueen(a, i))
		{
			board[a][i] = 1;
			pos[a] = i;
			setQueen(a + 1);
			board[a][i] = 0;
		}
	}

	return; // �����������.
}

int main()
{
	setQueen(0);

	cin.get();
	cin.get();

	return 0;
}